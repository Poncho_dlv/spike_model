#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_model.BaseJsonModel import BaseJsonModel
from spike_model.League import League
from typing import Dict


class Competition(BaseJsonModel):

    def __init__(self, json_data: Dict):
        super(Competition, self).__init__(json_data)
        self.league = League(self.data.get("league"))

    def get_name(self):
        return str(self.data.get("name")) # Cast to avoid some name as int

    def get_id(self):
        return self.data.get("id")

    def get_status(self):
        return self.data.get("status")

    def get_current_round(self):
        return self.data.get("round")

    def get_turn_duration(self):
        return self.data.get("turn_duration")

    def get_rounds_count(self):
        return self.data.get("rounds_count")

    def get_teams_count(self):
        return self.data.get("teams_count")

    def get_team_max(self):
        return self.data.get("teams_max")

    def get_format(self):
        return self.data.get("format")

    def get_date_created(self):
        return self.data.get("date_created")

    def get_league(self):
        return self.league
