#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
from spike_model.BaseJsonModel import BaseJsonModel
from spike_model.Team import Team
from spike_model.Coach import Coach
from spike_database.ResourcesRequest import ResourcesRequest
from typing import Dict


def get_inflicted_surfs(team):
    nb_surf = 0
    for player in team.players:
        nb_surf += player.get_inflicted_surfs()
    return nb_surf


class Match(BaseJsonModel):

    def __init__(self, json_data: Dict):
        super(Match, self).__init__(json_data.get("match"))

        self.team_home = Team(json_data.get("match"))
        self.team_home.init_team_from_match_ws(json_data.get("teams", [{}, {}])[0], True)

        self.coach_home = Coach(json_data.get("match", {}).get("coaches", [{}, {}])[0])

        self.team_away = Team(json_data.get("match"))
        self.team_away.init_team_from_match_ws(json_data.get("teams", [{}, {}])[1], False)

        self.coach_away = Coach(json_data.get("match", {}).get("coaches", [{}, {}])[1])

        self.impact_players = None
        rsrc_db = ResourcesRequest()
        self.data["platform_id"] = rsrc_db.get_platform_id(self.data.get("platform"))

    def set_uuid(self, uuid: str):
        self.data["uuid"] = uuid

    def get_platform_id(self):
        return self.data.get("platform_id")

    def set_league_logo(self, logo: str):
        self.data["leaguelogo"] = logo

    def get_league_logo(self):
        return self.data.get("leaguelogo")

    def set_competition_logo(self, logo: str):
        self.data["competitionlogo"] = logo

    def get_competition_logo(self):
        return self.data.get("competitionlogo")

    def get_uuid(self):
        return self.data.get("uuid")

    def get_platform(self):
        return self.data.get("platform")

    def get_coach_home(self):
        return self.coach_home

    def get_coach_away(self):
        return self.coach_away

    def get_team_home(self):
        return self.team_home

    def get_team_away(self):
        return self.team_away

    def get_competition_name(self):
        return str(self.data.get("competitionname")) # Cast to avoid some competition name as int

    def get_competition_id(self):
        return self.data.get("idcompetition")

    def get_league_name(self):
        return str(self.data.get("leaguename")) # Cast to avoid some league name as int

    def get_league_id(self):
        return self.data.get("idleague")

    def get_round(self):
        return self.data.get("round")

    def get_start_datetime(self, df: str = None):
        if df is None:
            ret = self.data.get("started")
        else:
            try:
                datetime_obj = datetime.datetime.strptime(self.data.get("started", ""), "%Y-%m-%d %H:%M:%S")
                ret = datetime_obj.strftime(df)
            except:
                ret = self.data.get("started")
        return ret

    def get_finish_datetime(self, df: str = None):
        if df is None:
            ret = self.data.get("finished")
        else:
            try:
                datetime_obj = datetime.datetime.strptime(self.data.get("finished", ""), "%Y-%m-%d %H:%M:%S")
                ret = datetime_obj.strftime(df)
            except:
                ret = self.data.get("finished")
        return ret

    def get_stadium_type(self):
        return self.data.get("stadium")

    def get_stadium_level(self):
        return self.data.get("levelstadium")

    def get_stadium_structure(self):
        return self.data.get("structstadium")

    def get_score_home(self):
        return self.data.get("teams", [{}, {}])[0].get("score")

    def get_score_away(self):
        return self.data.get("teams", [{}, {}])[1].get("score")

    def get_supporter_home(self):
        return self.data.get("teams", [{}, {}])[0].get("nbsupporters")

    def get_supporter_away(self):
        return self.data.get("teams", [{}, {}])[1].get("nbsupporters")

    def get_popularity_gain_home(self):
        return self.data.get("teams", [{}, {}])[0].get("popularitygain")

    def get_popularity_gain_away(self):
        return self.data.get("teams", [{}, {}])[1].get("popularitygain")

    def get_cash_spent_inducements_home(self):
        return self.data.get("teams", [{}, {}])[0].get("cashspentinducements")

    def get_cash_spent_inducements_away(self):
        return self.data.get("teams", [{}, {}])[1].get("cashspentinducements")

    def get_cash_earned_home(self):
        return self.data.get("teams", [{}, {}])[0].get("cashearned")

    def get_cash_earned_away(self):
        return self.data.get("teams", [{}, {}])[1].get("cashearned")

    def get_cash_earned_before_concession_home(self):
        return self.data.get("teams", [{}, {}])[0].get("cashearnedbeforeconcession")

    def get_cash_earned_before_concession_away(self):
        return self.data.get("teams", [{}, {}])[1].get("cashearnedbeforeconcession")

    def get_winnings_dice_home(self):
        return self.data.get("teams", [{}, {}])[0].get("winningsdice")

    def get_winnings_dice_away(self):
        return self.data.get("teams", [{}, {}])[1].get("winningsdice")

    def get_spiralling_expenses_home(self):
        return self.data.get("teams", [{}, {}])[0].get("spirallingexpenses")

    def get_spiralling_expenses_away(self):
        return self.data.get("teams", [{}, {}])[1].get("spirallingexpenses")

    def get_inflicted_blocks_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedtackles")

    def get_inflicted_blocks_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedtackles")

    def get_inflicted_AvBr_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedinjuries")

    def get_inflicted_AvBr_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedinjuries")

    def get_inflicted_ko_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedko")

    def get_inflicted_ko_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedko")

    def get_inflicted_casualties_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedcasualties")

    def get_inflicted_casualties_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedcasualties")

    def get_inflicted_dead_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflicteddead")

    def get_inflicted_dead_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflicteddead")

    def get_inflicted_surf_home(self):
        return get_inflicted_surfs(self.team_home)

    def get_inflicted_surf_away(self):
        return get_inflicted_surfs(self.team_away)

    def get_inflicted_passes_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedpasses")

    def get_inflicted_passes_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedpasses")

    def get_inflicted_catches_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedcatches")

    def get_inflicted_catches_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedcatches")

    def get_inflicted_touchdowns_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedtouchdowns")

    def get_inflicted_touchdowns_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedtouchdowns")

    def get_inflicted_interceptions_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedinterceptions")

    def get_inflicted_interceptions_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedinterceptions")

    def get_inflicted_meters_running_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedmetersrunning")

    def get_inflicted_meters_running_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedmetersrunning")

    def get_inflicted_meters_passing_home(self):
        return self.data.get("teams", [{}, {}])[0].get("inflictedmeterspassing")

    def get_inflicted_meters_passing_away(self):
        return self.data.get("teams", [{}, {}])[1].get("inflictedmeterspassing")

    def get_sustained_expulsions_home(self):
        return self.data.get("teams", [{}, {}])[0].get("sustainedexpulsions")

    def get_sustained_expulsions_away(self):
        return self.data.get("teams", [{}, {}])[1].get("sustainedexpulsions")

    def get_possession_home(self):
        return self.data.get("teams", [{}, {}])[0].get("possessionball")

    def get_possession_away(self):
        return self.data.get("teams", [{}, {}])[1].get("possessionball")

    def get_occupation_own_home(self):
        return self.data.get("teams", [{}, {}])[0].get("occupationown")

    def get_occupation_own_away(self):
        return self.data.get("teams", [{}, {}])[1].get("occupationown")

    def get_occupation_their_home(self):
        return self.data.get("teams", [{}, {}])[0].get("occupationtheir")

    def get_occupation_their_away(self):
        return self.data.get("teams", [{}, {}])[1].get("occupationtheir")

    def get_impact_player(self):
        if self.impact_players is not None:
            return self.impact_players
        self.impact_players = []
        # Compute impact players
        for player in self.get_team_home().get_players():
            self.impact_players.append((player.get_impact_player_value(), player))

        for player in self.get_team_away().get_players():
            self.impact_players.append((player.get_impact_player_value(), player))

        self.impact_players = sorted(self.impact_players, reverse=True)
        return self.impact_players

    def get_winner_coach(self):
        if self.get_score_home() == self.get_score_away():
            return None
        if self.get_score_home() > self.get_score_away():
            return self.get_coach_home()
        return self.get_coach_away()

    def get_winner_team(self):
        if self.get_score_home() == self.get_score_away():
            return None
        if self.get_score_home() > self.get_score_away():
            return self.get_team_home()
        return self.get_team_away()

    def is_resurrection(self):
        if self.team_home.get_winnings_dice() == self.team_away.get_winnings_dice() == 0:
            return True
        else:
            return False
