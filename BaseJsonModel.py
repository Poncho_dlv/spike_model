#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict


class BaseJsonModel:

    def __init__(self, json_data: Dict):
        self.data = json_data
