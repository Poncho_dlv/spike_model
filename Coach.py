#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict

from spike_model.BaseJsonModel import BaseJsonModel


class Coach(BaseJsonModel):

    def __init__(self, json_data: Dict):
        super(Coach, self).__init__(json_data)

    def get_id(self):
        return self.data.get("idcoach", self.data.get("id")) if self.data is not None else None

    def get_name(self):
        coach_name = self.data.get("coachname", self.data.get("name"))
        if coach_name is not None:
            return str(coach_name)
        return None

    def get_lang(self):
        return self.data.get("lastlang", self.data.get("lang"))

    def get_country(self):
        return self.data.get("country")

    def get_created_date(self):
        return self.data.get("created")

    def get_cyan_earned(self):
        return self.data.get("coachcyanearned")

    def get_xp_earned(self):
        return self.data.get("coachxpearned")

