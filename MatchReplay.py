#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict


def get_value(key, data):
    if key in data:
        return data[key]
    return None


class MatchReplay:

    def __init__(self):
        self.team_home = {}

        self.team_away = {}

    # Home team
    def set_coach_home(self, coach_home):
        self.team_home["coach_home"] = coach_home

    def get_coach_home(self):
        return get_value("coach_home", self.team_home)

    def set_coach_home_id(self, coach_home_id):
        self.team_home["coach_home_id"] = coach_home_id

    def get_coach_home_id(self):
        return get_value("coach_home_id", self.team_home)

    # Away team
    def set_coach_away(self, coach_away):
        self.team_home["coach_away"] = coach_away

    def get_coach_away(self):
        return get_value("coach_away", self.team_away)

    def set_coach_away_id(self, coach_away_id):
        self.team_home["coach_away_id"] = coach_away_id

    def get_coach_away_id(self):
        return get_value("coach_away_id", self.team_away)
