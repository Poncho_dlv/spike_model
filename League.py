#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_model.BaseJsonModel import BaseJsonModel
from spike_database.ResourcesRequest import ResourcesRequest
from typing import Dict


class League(BaseJsonModel):

    def __init__(self, json_data: Dict, platform: str = "pc"):
        super(League, self).__init__(json_data)
        self.platform = platform
        rsrc_db = ResourcesRequest()
        self.platform_id = rsrc_db.get_platform_id(platform)

    def get_name(self):
        return str(self.data.get("name"))  # Cast to avoid some name as int

    def get_id(self):
        return self.data.get("id")

    def get_date_created(self):
        return self.data.get("date_created")

    def get_logo(self):
        return self.data.get("logo")

    def is_official(self):
        if self.data.get("official") is None:
            return None
        return self.data.get("official") == 1

    def get_teams_count(self):
        return self.data.get("registered_teams_count", self.data.get("team_count"))

    def get_last_match(self):
        return self.data.get("date_last_match")

    def get_treasury(self):
        return self.data.get("treasury")

    def set_platform(self, platform: str):
        self.platform = platform

    def get_platform(self):
        return self.platform

    def get_platform_id(self):
        return self.platform_id

    def set_platform_id(self, platform_id: int):
        self.platform_id = platform_id
