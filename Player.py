#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.ResourcesRequest import ResourcesRequest
from spike_database.BB2Resources import BB2Resources
from operator import xor
from spike_model.BaseJsonModel import BaseJsonModel
import math
from typing import Dict


class Player(BaseJsonModel):

    def __init__(self, json_data: Dict):
        super(Player, self).__init__(json_data)

        self.impact_player_value = None
        self.impact_data = None
        self.__is_mng = False

    def get_id(self):
        return self.data.get("id")

    def get_name(self, lang: str = "en"):
        if self.is_champion():
            rsrc_db = ResourcesRequest()
            return rsrc_db.get_champion_name_translation(str(self.data.get("name")), lang)
        else:
            return str(self.data.get("name"))  # Cast to avoid some name as int

    def get_number(self):
        return self.data.get("number")

    def get_player_value(self):
        return self.data.get("value")

    def is_mvp(self):
        return self.data.get("mvp", 0) > 0

    def get_base_ma(self):
        return int(self.data.get("attributes", {}).get("ma"))

    def get_ma(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")
        ma_modification = 0
        ma_modification -= casualties_state.count(12)
        ma_modification -= casualties_state.count(13)
        ma_modification += skill_state.count("IncreaseMovement")
        return int(self.data.get("attributes", {}).get("ma", 0)) + ma_modification

    def get_ma_modification(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")
        ma_modification = 0
        ma_modification -= casualties_state.count(12)
        ma_modification -= casualties_state.count(13)
        ma_modification += skill_state.count("IncreaseMovement")
        return ma_modification

    def get_base_st(self):
        return int(self.data.get("attributes", {}).get("st"))

    def get_st(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")
        st_modification = 0
        st_modification -= casualties_state.count(17)
        st_modification += skill_state.count("IncreaseStrength")
        return int(self.data.get("attributes", {}).get("st", 0)) + st_modification

    def get_st_modification(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")
        st_modification = 0
        st_modification -= casualties_state.count(17)
        st_modification += skill_state.count("IncreaseStrength")
        return st_modification

    def get_base_ag(self):
        return int(self.data.get("attributes", {}).get("ag"))

    def get_ag(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")
        ag_modification = 0
        ag_modification -= casualties_state.count(16)
        ag_modification += skill_state.count("IncreaseAgility")
        return int(self.data.get("attributes", {}).get("ag", 0)) + ag_modification

    def get_ag_modification(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")
        ag_modification = 0
        ag_modification -= casualties_state.count(16)
        ag_modification += skill_state.count("IncreaseAgility")
        return ag_modification

    def get_base_av(self):
        return int(self.data.get("attributes", {}).get("av"))

    def get_av(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")
        av_modification = 0
        av_modification -= casualties_state.count(14)
        av_modification -= casualties_state.count(15)
        av_modification += skill_state.count("IncreaseArmour")
        return int(self.data.get("attributes", {}).get("av", 0)) + av_modification

    def get_av_modification(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")
        av_modification = 0
        av_modification -= casualties_state.count(14)
        av_modification -= casualties_state.count(15)
        av_modification += skill_state.count("IncreaseArmour")
        return av_modification

    def get_attributes(self):
        casualties_state = self.data.get("casualties_state_id")
        skill_state = self.data.get("skills")

        ma_modification = 0
        ma_modification -= casualties_state.count(12)
        ma_modification -= casualties_state.count(13)
        ma_modification += skill_state.count("IncreaseMovement")
        ma = int(self.data.get("attributes", {}).get("ma", 0)) + ma_modification

        st_modification = 0
        st_modification -= casualties_state.count(17)
        st_modification += skill_state.count("IncreaseStrength")
        st = int(self.data.get("attributes", {}).get("st", 0)) + st_modification

        ag_modification = 0
        ag_modification -= casualties_state.count(16)
        ag_modification += skill_state.count("IncreaseAgility")
        ag = int(self.data.get("attributes", {}).get("ag", 0)) + ag_modification

        av_modification = 0
        av_modification -= casualties_state.count(14)
        av_modification -= casualties_state.count(15)
        av_modification += skill_state.count("IncreaseArmour")
        av = int(self.data.get("attributes", {}).get("av", 0)) + av_modification

        attr = "{} {} {} {}".format(ma, st, ag, av)
        return attr

    def get_type(self, lang: str = "en"):
        if self.is_champion():
            return "Star Player"
        else:
            rsrc_db = ResourcesRequest()
            return rsrc_db.get_player_type_translation(self.data.get("type", "_").split("_")[-1], lang)

    def get_type_id(self):
        return self.data.get("type")

    def get_level(self):
        return self.data.get("level")

    def get_raw_attributes(self):
        return self.data.get("attributes")

    def get_raw_casualties_state_id(self):
        return self.data.get("casualties_state_id")

    def get_raw_casualties_state(self):
        return self.data.get("casualties_state")

    def get_skills_emoji(self):
        skills = self.data.get("skills")
        skills_emoji = []
        db = ResourcesRequest()
        for skill in skills:
            skills_emoji.append(db.get_skill_emoji(skill))
        return skills_emoji

    def get_skills(self):
        return self.data.get("skills")

    def get_starting_skills(self):
        bb_db = BB2Resources()
        list_of_skills = []
        player_id = bb_db.get_player_id(self.get_type_id())
        skills_id = bb_db.get_skills_id(player_id)
        for skill_id in skills_id:
            skill_name = bb_db.get_skills_name(skill_id[0])
            list_of_skills.append(skill_name)
        return list_of_skills

    def get_spp(self):
        return self.data.get("xp")

    def get_spp_gain(self):
        return self.data.get("xp_gain")

    def is_badly_hurt_only(self):
        if len(self.data.get("casualties_sustained_id")) > 0 and max(self.data.get("casualties_sustained_id")) == 1:
            return True
        return False

    def is_mng(self):
        if (self.data.get("casualties_sustained_id") and len(self.data.get("casualties_sustained_id")) > 0 and 1 < max(self.data.get("casualties_sustained_id")) < 18) or self.__is_mng:
            return True
        return False

    def set_is_mng(self, is_mng):
        self.__is_mng = is_mng

    def is_injured(self):
        if len(self.data.get("casualties_sustained_id")) == 0 or max(self.data.get("casualties_sustained_id")) == 1:
            return False
        return True

    def is_dead(self):
        if len(self.data.get("casualties_sustained_id")) > 0 and 18 in self.data.get("casualties_sustained_id"):
            return True
        return False

    def get_casualties_sustained(self):
        db = ResourcesRequest()
        case_emoji = []
        for case_id in self.data.get("casualties_sustained_id"):
            case_emoji.append(db.get_casualtie_emoji(case_id))
        return case_emoji

    def get_casualties_sustained_label(self):
        db = ResourcesRequest()
        case_list = []
        for case_id in self.data.get("casualties_sustained_id"):
            if case_id > 1:
                label = db.get_casualtie_emoji(case_id).split(":")[1]
                case_list.append(label)
        return case_list

    def get_casualties_state(self):
        db = ResourcesRequest()
        case_emoji = ""
        for case_id in self.data.get("casualties_state_id"):
            case_emoji += db.get_casualtie_emoji(case_id)
        return case_emoji

    def get_casualties_state_label(self):
        db = ResourcesRequest()
        case_list = []
        for case_id in self.data.get("casualties_state_id"):
            if case_id > 1:
                label = db.get_casualtie_emoji(case_id).split(":")[1]
                case_list.append(label)
        return case_list

    def is_levelling_up(self):
        trigger_points = (6, 16, 31, 51, 76, 176)

        if self.get_spp_gain() is not None:
            # Match case
            pre = self.get_spp()
            post = pre + self.get_spp_gain()

            # Which trigger points are passed pre and post match
            pre_match = (pre >= tp for tp in trigger_points)
            post_match = (post >= tp for tp in trigger_points)

            # logical xor returns True only if there is a difference between pre and post match
            diff = map(xor, pre_match, post_match)

            # If any elements of diff are True, have leveled up
            return any(diff)
        else:
            # Team case
            spp = self.get_spp()
            level = self.get_level()
            if level < 7 and spp > trigger_points[level-1]:
                return True
            return False

    def get_next_lvl(self):
        current_lvl = self.data.get("level", 1)

        # Should only have a level between 1 and 7
        if current_lvl not in range(1, 8):
            return None

        return (6, 16, 31, 51, 76, 176, '*')[current_lvl - 1]

    def get_match_played(self):
        return self.data.get("matchplayed")

    def get_nb_passes(self):
        return self.data.get("stats", {}).get("inflictedpasses")

    def get_passes_distance(self):
        return self.data.get("stats", {}).get("inflictedmeterspassing")

    def get_nb_catches(self):
        return self.data.get("stats", {}).get("inflictedcatches")

    def get_nb_interceptions(self):
        return self.data.get("stats", {}).get("inflictedinterceptions")

    def get_inflicted_surfs(self):
        return self.data.get("stats", {}).get("inflictedpushouts")

    def get_carrying_distance(self):
        return self.data.get("stats", {}).get("inflictedmetersrunning")

    def get_nb_touchdowns(self):
        return self.data.get("stats", {}).get("inflictedtouchdowns")

    def get_inflicted_Block(self):
        return self.data.get("stats", {}).get("inflictedtackles")

    def get_inflicted_AvBr(self):
        return self.data.get("stats", {}).get("inflictedinjuries")

    def get_inflicted_casualties(self):
        return self.data.get("stats", {}).get("inflictedcasualties")

    def get_inflicted_stuns(self):
        return self.data.get("stats", {}).get("inflictedstuns")

    def get_inflicted_Ko(self):
        return self.data.get("stats", {}).get("inflictedko")

    def get_inflicted_dead(self):
        return self.data.get("stats", {}).get("inflicteddead")

    def is_champion(self):
        if "PLAYER_NAMES_CHAMPION_" in str(self.data.get("name")):
            return True
        return False

    def get_impact_player_value(self):
        if self.impact_player_value is None:
            self.impact_data = {
                "Blocks":           math.ceil(self.get_inflicted_Block() / 5.),
                "Armour Breaks":    math.ceil(self.get_inflicted_AvBr() / 3.),
                "Kos":              self.get_inflicted_Ko(),
                "Casualties":       self.get_inflicted_casualties(),
                "Kills":            self.get_inflicted_dead()*2,
                "Surfs":            self.get_inflicted_surfs()*2,
                "Passes":           (self.get_nb_passes()*2) + math.ceil(self.get_passes_distance() / 20.),
                "Catches":          self.get_nb_catches()*2,
                "Interceptions":    self.get_nb_interceptions()*5,
                "Carrying":         math.ceil(self.get_carrying_distance() / 50.),
                "Touchdowns":       self.get_nb_touchdowns()*5
            }

            self.impact_player_value = sum(self.impact_data.values())
        return self.impact_player_value

    def get_impact_player_stats(self):
        top_3 = sorted(self.impact_data, key=self.impact_data.get, reverse=True)[:3]
        output = ""

        for entry in top_3:
            if entry == "Blocks":
                output += " Blk: {}".format(self.get_inflicted_Block())
            elif entry == "Armour Breaks":
                output += " AvBr: {}".format(self.get_inflicted_AvBr())
            elif entry == "Kos":
                output += " Ko: {}".format(self.get_inflicted_Ko())
            elif entry == "Casualties":
                output += " Cas: {}".format(self.get_inflicted_casualties())
            elif entry == "Kills":
                output += " Kill: {}".format(self.get_inflicted_dead())
            elif entry == "Surfs":
                output += " Surf: {}".format(self.get_inflicted_surfs())
            elif entry == "Passes":
                output += " Pass.: {} ({} yds)".format(self.get_nb_passes(), self.get_passes_distance())
            elif entry == "Catches":
                output += " Catch: {}".format(self.get_nb_catches())
            elif entry == "Interceptions":
                output += " Int.: {}".format(self.get_nb_interceptions())
            elif entry == "Carrying":
                output += " Carry.: {} yds".format(self.get_carrying_distance())
            elif entry == "Touchdowns":
                output += " TD: {}".format(self.get_nb_touchdowns())

        return output

    def __lt__(self, other):
        if self.get_impact_player_value() < other.get_impact_player_value():
            return True
        elif self.get_impact_player_value() == other.get_impact_player_value():
            if self.get_spp_gain() < other.get_spp_gain():
                return True
            elif self.get_spp_gain() == other.get_spp_gain():
                if self.get_spp() < other.get_spp():
                    return True
        return False



