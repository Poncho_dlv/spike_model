#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_model.BaseJsonModel import BaseJsonModel
from spike_model.Player import Player
from spike_model.Coach import Coach
from spike_database.BB2Resources import BB2Resources
from spike_database.Coaches import Coaches
from typing import Dict


class Team(BaseJsonModel):

    def __init__(self, json_data: Dict):
        super(Team, self).__init__(json_data)
        self.players = []
        self.coach = None
        self.team_data = {}

    def update_players_status(self, match_model):
        if match_model.get_team_home().get_id() == self.get_id():
            team = match_model.get_team_home()
        else:
            team = match_model.get_team_away()

        for player in self.players:
            match_player = team.get_player(player.get_id())
            if match_player is not None:
                player.set_is_mng(match_player.is_mng())

    def init_team_from_database(self):
        self.team_data["id"] = self.data.get("id")
        self.team_data["name"] = self.data.get("name")
        self.team_data["logo"] = self.data.get("logo")
        self.team_data["idraces"] = self.data.get("race_id")
        self.team_data["apothecary"] = self.data.get("apothecary")
        self.team_data["assistantcoaches"] = self.data.get("assistant_coaches")
        self.team_data["cash"] = self.data.get("cash")
        self.team_data["cheerleaders"] = self.data.get("cheerleaders")
        self.team_data["created"] = self.data.get("created")
        self.team_data["datelastmatch"] = self.data.get("last_match")
        self.team_data["teamcolor"] = self.data.get("color")
        self.team_data["motto"] = self.data.get("leitmotiv")
        self.team_data["popularity"] = self.data.get("popularity")
        self.team_data["rerolls"] = self.data.get("rerolls")
        self.team_data["value"] = self.data.get("value")
        self.team_data["stadiumname"] = self.data.get("stadium_name")
        self.team_data["stadiumlevel"] = self.data.get("stadium_level")
        self.team_data["stadiumtype"] = self.data.get("stadium_type")
        bb_rsrc_db = BB2Resources()
        race_label = bb_rsrc_db.get_race_label(self.data.get("race_id"))
        self.team_data["race_label"] = ''.join(map(lambda x: x if x.islower() else " " + x, race_label))

        coach_db = Coaches()
        coach_data = coach_db.get_coach_data(coach_id=self.data.get("coach_id"), platform_id=self.data.get("platform_id"))
        if coach_data is not None:
            self.coach = Coach(coach_data)
        else:
            self.coach = Coach({"id": self.data.get("coach_id")})

    def init_team_from_contests_ws(self):
        self.team_data["id"] = self.data.get("team", {}).get("id")
        self.team_data["name"] = self.data.get("team", {}).get("name", self.data.get("team", {}).get("team_name", None))
        self.team_data["logo"] = self.data.get("team", {}).get("logo", self.data.get("team", {}).get("team_logo", None))
        self.team_data["value"] = self.data.get("team", {}).get("value")
        self.team_data["motto"] = self.data.get("team", {}).get("motto")
        self.team_data["score"] = self.data.get("team", {}).get("score")
        self.team_data["death"] = self.data.get("team", {}).get("death")
        self.team_data["race"] = self.data.get("team", {}).get("race", None)

        if self.team_data["race"] is not None:
            bb_rsrc_db = BB2Resources()
            self.team_data["idraces"] = bb_rsrc_db.get_race_id(self.team_data["race"])
            self.team_data["race_label"] = ''.join(map(lambda x: x if x.islower() else " " + x, self.team_data.get("race")))

    def init_team_from_team_ws(self):
        self.team_data["id"] = self.data.get("team", {}).get("id")
        self.team_data["name"] = self.data.get("team", {}).get("name")
        self.team_data["idraces"] = self.data.get("team", {}).get("idraces")
        self.team_data["logo"] = self.data.get("team", {}).get("logo")
        self.team_data["value"] = self.data.get("team", {}).get("value")
        self.team_data["popularity"] = self.data.get("team", {}).get("popularity")
        self.team_data["cash"] = self.data.get("team", {}).get("cash")
        self.team_data["cheerleaders"] = self.data.get("team", {}).get("cheerleaders")
        self.team_data["apothecary"] = self.data.get("team", {}).get("apothecary")
        self.team_data["rerolls"] = self.data.get("team", {}).get("rerolls")
        self.team_data["assistantcoaches"] = self.data.get("team", {}).get("assistantcoaches")
        self.team_data["nbplayers"] = self.data.get("team", {}).get("nbplayers")
        self.team_data["created"] = self.data.get("team", {}).get("created")
        self.team_data["datelastmatch"] = self.data.get("team", {}).get("datelastmatch")
        self.team_data["teamcolor"] = self.data.get("team", {}).get("teamcolor")
        self.team_data["stadiumname"] = self.data.get("team", {}).get("stadiumname", "")
        self.team_data["stadiumlevel"] = self.data.get("team", {}).get("stadiumlevel", 1)
        self.team_data["stadiumtype"] = self.data.get("team", {}).get("stadiumtype", "")

        bb_rsrc_db = BB2Resources()
        self.team_data["race"] = bb_rsrc_db.get_race_label(self.team_data["idraces"])
        self.team_data["race_label"] = ''.join(map(lambda x: x if x.islower() else " " + x, self.team_data.get("race")))

        for player in self.data.get("roster", []):
            player = Player(player)
            self.players.append(player)

        self.players = sorted(self.players, key=lambda i: i.get_number())

        self.coach = Coach(self.data.get("coach"))

        self.data = None

    def init_team_from_match_ws(self, extra_team_data: Dict, init_from_home_team: bool = True):
        if init_from_home_team is True:
            index = 0
        else:
            index = 1

        self.team_data["id"] = self.data.get("teams", [{}, {}])[index].get("idteamlisting")
        self.team_data["name"] = self.data.get("teams", [{}, {}])[index].get("teamname")
        self.team_data["idraces"] = self.data.get("teams", [{}, {}])[index].get("idraces")
        self.team_data["logo"] = self.data.get("teams", [{}, {}])[index].get("teamlogo")
        self.team_data["value"] = self.data.get("teams", [{}, {}])[index].get("value")
        self.team_data["mvp"] = self.data.get("teams", [{}, {}])[index].get("mvp")
        self.team_data["winningsdice"] = self.data.get("teams", [{}, {}])[index].get("winningsdice")
        self.team_data["motto"] = extra_team_data.get("leitmotiv")
        self.team_data["popularity"] = extra_team_data.get("popularity", 0)
        self.team_data["cash"] = extra_team_data.get("cash", 0)
        self.team_data["cheerleaders"] = extra_team_data.get("cheerleaders", 0)
        self.team_data["apothecary"] = extra_team_data.get("apothecary", 0)
        self.team_data["balms"] = extra_team_data.get("balms", 0)
        self.team_data["rerolls"] = extra_team_data.get("rerolls", 0)
        self.team_data["assistantcoaches"] = extra_team_data.get("assistantcoaches", 0)
        self.team_data["nbplayers"] = extra_team_data.get("nbplayers", 0)
        self.team_data["created"] = extra_team_data.get("created")
        self.team_data["datelastmatch"] = extra_team_data.get("datelastmatch")
        self.team_data["teamcolor"] = extra_team_data.get("teamcolor")
        self.team_data["stadiumname"] = extra_team_data.get("stadiumname", "")
        self.team_data["stadiumlevel"] = extra_team_data.get("stadiumlevel", 1)
        self.team_data["stadiumtype"] = extra_team_data.get("stadiumtype", "")

        bb_rsrc_db = BB2Resources()
        self.team_data["race"] = bb_rsrc_db.get_race_label(self.team_data["idraces"])
        self.team_data["race_label"] = ''.join(map(lambda x: x if x.islower() else " " + x, self.team_data.get("race")))

        if len(self.data.get("teams", [])) > 0 and "roster" in self.data.get("teams", [{}, {}])[index].keys() and self.data.get("teams", [{}, {}])[index].get("roster") is not None:
            for player in self.data.get("teams", [{}, {}])[index].get("roster"):
                player = Player(player)
                self.players.append(player)
            self.players = sorted(self.players, key=lambda i: i.get_number())

        self.coach = Coach(self.data.get("coaches", [{}, {}])[index])

        self.data = None

    def get_balms(self):
        return self.team_data.get("balms")

    def get_team_color(self):
        return self.team_data.get("teamcolor")

    def get_stadium_name(self):
        return self.team_data.get("stadiumname")

    def get_stadium_level(self):
        return self.team_data.get("stadiumlevel")

    def get_stadium_type(self):
        return self.team_data.get("stadiumtype")

    def get_id(self):
        return self.team_data.get("id")

    def get_name(self):
        return str(self.team_data.get("name"))  # Cast to avoid some name as int

    def get_creation_date(self):
        return self.team_data.get("created")

    def get_date_last_match(self):
        return self.team_data.get("datelastmatch")

    def get_logo(self):
        return self.team_data.get("logo")

    def get_team_value(self):
        return self.team_data.get("value")

    def get_popularity(self):
        return self.team_data.get("popularity")

    def get_cash(self):
        return self.team_data.get("cash")

    def get_nb_cheerleaders(self):
        return self.team_data.get("cheerleaders")

    def get_apothecary(self):
        return self.team_data.get("apothecary")

    def get_nb_rerolls(self):
        return self.team_data.get("rerolls")

    def get_nb_assistant(self):
        return self.team_data.get("assistantcoaches")

    def get_nb_player(self):
        return self.team_data.get("nbplayers")

    def get_player(self, player_id: int, player_name: str = None, lang: str = "en"):
        for player in self.players:
            if player_id is not None and player.get_id() == player_id:
                return player
            elif player_name is not None and player_name == player.get_name(lang):
                return player
        return None

    def get_players(self):
        return self.players

    def get_coach(self):
        return self.coach

    def get_race_id(self):
        return self.team_data.get("idraces")

    def get_earned_spp(self):
        spp = 0
        for player in self.get_players():
            spp += player.get_spp_gain()

        return spp

    def get_total_spp(self):
        spp = 0
        for player in self.get_players():
            spp += player.get_spp()

        return spp

    def get_motto(self):
        return self.team_data.get("motto")

    def get_score(self):
        return self.team_data.get("score")

    def get_nb_death(self):
        return self.team_data.get("death")

    def get_race(self):
        return self.team_data.get("race")

    def get_race_label(self):
        return self.team_data.get("race_label")

    def get_nb_mvp(self):
        return self.team_data.get("mvp")

    def get_winnings_dice(self):
        return self.team_data.get("winningsdice")
