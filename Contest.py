#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_model.BaseJsonModel import BaseJsonModel
from spike_model.Coach import Coach
from spike_model.Team import Team
from enum import IntEnum
from typing import Dict


class ContestStatus(IntEnum):
    scheduled = 0
    in_progress = 1
    played = 2


class Contest(BaseJsonModel):

    def __init__(self, json_data: Dict):
        super(Contest, self).__init__(json_data)
        self.status_dict = {"scheduled": ContestStatus.scheduled, "in_progress": ContestStatus.in_progress, "played": ContestStatus.played}

        if self.data.get("opponents", [{}, {}]) is None:
            self.data["opponents"] = [{}, {}]
        self.coach_home = Coach(self.data.get("opponents", [{}, {}])[0].get("coach"))
        self.coach_away = Coach(self.data.get("opponents", [{}, {}])[1].get("coach"))

        self.team_home = Team(self.data.get("opponents", [{}, {}])[0])
        self.team_home.init_team_from_contests_ws()

        self.team_away = Team(self.data.get("opponents", [{}, {}])[1])
        self.team_away.init_team_from_contests_ws()

        # Init winner
        if self.data.get("winner") is not None:
            self.coach_winner = Coach(self.data.get("winner", {}).get("coach"))
            self.team_winner = Team(self.data.get("winner", {}))
            self.team_winner.init_team_from_contests_ws()
        else:
            self.coach_winner = None
            self.team_winner = None

    def get_league_name(self):
        return self.data.get("league", self.data.get("league_name"))

    def get_league_id(self):
        return self.data.get("league_id")

    def get_competition_name(self):
        return self.data.get("competition", self.data.get("competition_name"))

    def get_competition_logo(self):
        return self.data.get("competition_logo")

    def set_competition_logo(self, logo):
        self.data["competition_logo"] = logo

    def get_competition_id(self):
        return self.data.get("competition_id")

    def get_contest_id(self):
        return self.data.get("contest_id")

    def get_platform_id(self):
        return self.data.get("platform_id")

    def get_date(self):
        return self.data.get("date")

    def set_date(self, date):
        self.data["date"] = date

    def get_format(self):
        return self.data.get("format")

    def get_round(self):
        return self.data.get("round",  self.data.get("current_round")) if self.data is not None else None

    def get_type(self):
        return self.data.get("type")

    def get_status(self):
        return self.status_dict.get(self.data.get("status"))

    def get_status_label(self):
        return self.data.get("status")

    def get_stadium(self):
        return self.data.get("stadium")

    def get_match_uuid(self):
        return self.data.get("match_uuid")

    def get_match_id(self):
        return self.data.get("match_id")

    def get_coach_home(self):
        return self.coach_home

    def get_coach_away(self):
        return self.coach_away

    def get_team_home(self):
        return self.team_home

    def get_team_away(self):
        return self.team_away

    def get_coach_winner(self):
        return self.coach_winner

    def get_team_winner(self):
        return self.team_winner
